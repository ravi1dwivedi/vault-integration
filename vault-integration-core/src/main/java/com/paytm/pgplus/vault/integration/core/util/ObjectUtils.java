package com.paytm.pgplus.vault.integration.core.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ObjectUtils {

    private static final ObjectMapper flexibleMapper = flexibleMapper();
    private static final ObjectMapper strictMapper = strictMapper();

    private static ObjectMapper flexibleMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper;
    }

    private static ObjectMapper strictMapper() {
        return new ObjectMapper();
    }

    public static <I, O> O convert(I inputObject, Class<O> outputObjectClass) {
        Objects.requireNonNull(inputObject);
        return flexibleMapper.convertValue(inputObject, outputObjectClass);
    }

    public static <I, O> O convertStrict(I inputObject, Class<O> outputObjectClass) {
        Objects.requireNonNull(inputObject);
        return strictMapper.convertValue(inputObject, outputObjectClass);
    }

    public static <O> O jsonToObject(String inputObject, Class<O> outputObjectClass) {
        Objects.requireNonNull(inputObject);
        try {
            return flexibleMapper.readValue(inputObject, outputObjectClass);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static <I, O> List<O> convert(List<I> inputObjects, Class<O> outputObjectClass) {
        Objects.requireNonNull(inputObjects);
        List<O> output = new ArrayList<>();
        inputObjects.forEach(inputObject -> output.add(convert(inputObject, outputObjectClass)));
        return output;
    }

    public static <I, O> List<O> convertStrict(List<I> inputObjects, Class<O> outputObjectClass) {
        Objects.requireNonNull(inputObjects);
        List<O> output = new ArrayList<>();
        inputObjects.forEach(inputObject -> output.add(convertStrict(inputObject, outputObjectClass)));
        return output;
    }

    public static String writeToString(Object object) throws IOException {
        return flexibleMapper.writeValueAsString(object);
    }

    public static String prettyPrint(Object o) {
        Objects.requireNonNull(o);
        try {
            return flexibleMapper.writerWithDefaultPrettyPrinter().writeValueAsString(o);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return o.toString();
    }

    public static Object deepClone(Object object) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            return ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
