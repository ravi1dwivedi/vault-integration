package com.paytm.pgplus.vault.integration.core.util;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;


public class HTTPUtils {

    private static final Logger LOG = LoggerFactory.getLogger(HTTPUtils.class);

    public static String requestGet(String url, Map<String, String> headers, Map<String, Object> parameters) {

        try {
            LOG.info(" requestGet Request: {} , Request Params: {}", url, parameters);
            HttpResponse<String> resp = Unirest.get(url)
                    .headers(headers)
                    .queryString(parameters)
                    .asString();
            String body = resp.getBody();
            int status = resp.getStatus();
            LOG.info(" requestGet Response: [status={}] [body={}]", status, body);
            if (status != 200) {
                LOG.error("requestPost Error response received: " + resp.getStatusText());
                //throw new HTTPRequestException(status, body);
            }
            return body;
        } catch (Exception e) {
            //throw e;
        }

        return null;
    }

    public static String requestPost(String url, Map<String, String> headers, Map<String, Object> parameters) throws Exception {
        try {
            LOG.info(" requestPost Request: {}", url);
            HttpResponse<String> resp = Unirest.post(url)
                    .headers(headers)
                    .queryString(parameters)
                    .asString();
            String body = resp.getBody();
            int status = resp.getStatus();
            LOG.info("requestPost Response: [status={}] [body={}]", status, resp.getBody());
            if (status != 200) {
                LOG.error("requestPost Error response received: " + resp.getStatusText());
                // throw new HTTPRequestException(status, body);
            }
            //return ObjectUtils.jsonToObject(body, clazz);
        } catch (Exception e) {
            // throw e;
        }

        return null;
    }
}