package com.paytm.pgplus.vault.integration.condition;

import com.paytm.pgplus.vault.integration.service.VaultService;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class VaultEnabledCondition implements Condition {

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        VaultService vaultService = context.getBeanFactory().getBean(VaultService.class);
        if (vaultService != null)
            return vaultService.isVaultEnabled();
        return Boolean.FALSE;
    }
}
