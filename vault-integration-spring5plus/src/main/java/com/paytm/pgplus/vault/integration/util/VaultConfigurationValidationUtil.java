package com.paytm.pgplus.vault.integration.util;

import com.paytm.pgplus.vault.integration.exception.VaultConfigurationException;
import org.apache.commons.lang3.StringUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

public class VaultConfigurationValidationUtil {

    public static boolean validate(String endPoint, Optional<String> dbPropertiesPath, Optional<String> staticPropertiesPath) throws VaultConfigurationException {
        if (StringUtils.isEmpty(endPoint))
            throw new VaultConfigurationException("endPoint", "cannot be empty");
        try {
            new URI(endPoint);
        } catch (URISyntaxException e) {
            throw new VaultConfigurationException("endPoint", e.getMessage());
        }
        if (dbPropertiesPath == null)
            throw new VaultConfigurationException("dbProperties optional", "cannot be null");
        if (staticPropertiesPath == null)
            throw new VaultConfigurationException("staticProperties optional", "cannot be null");
        return true;
    }

    public static boolean validateRoleAndSecretId(String roleId, String secretId) throws VaultConfigurationException {
        if (StringUtils.isEmpty(roleId))
            throw new VaultConfigurationException("roleId", "cannot be empty");
        if (StringUtils.isEmpty(secretId))
            throw new VaultConfigurationException("secretId", "cannot be empty");
        return true;
    }
}
