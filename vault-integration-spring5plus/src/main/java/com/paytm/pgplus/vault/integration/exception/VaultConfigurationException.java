package com.paytm.pgplus.vault.integration.exception;

public class VaultConfigurationException extends Exception {

    private String field;

    private String validationFailedMessage;

    public VaultConfigurationException(String field, String validationFailedMessage) {
        this.field = field;
        this.validationFailedMessage = validationFailedMessage;
    }

    @Override
    public String getMessage() {
        return "Vault Configuration Not Valid for [Field]-" + field + " [Message]-" + validationFailedMessage;
    }
}
