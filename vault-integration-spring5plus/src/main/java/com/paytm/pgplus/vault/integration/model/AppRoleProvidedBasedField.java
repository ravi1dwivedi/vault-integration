package com.paytm.pgplus.vault.integration.model;

import com.paytm.pgplus.vault.integration.constant.Constants;
import com.paytm.pgplus.vault.integration.constant.VaultConfigurationType;

public enum AppRoleProvidedBasedField implements IVaultConfigurationField {

    VAULT_ENDPOINT("vaultEndPoint", Constants.VAULT_URI, true),

    DB_PROPERTIES_PATH("dbPropertiesPath", Constants.VAULT_DATABASE_PROPERTIES_PATH, false),

    STATIC_PROPERTIES_PATH("staticPropertiesPath", Constants.VAULT_STATIC_PROPERTIES_PATH, false),

    ROLE_ID("roleId", Constants.VAULT_ROLE_ID, true),

    SECRET_ID("secretId", Constants.VAULT_SECRET_ID, true);

    private String fieldKey;
    private String fieldName;
    private boolean isMandatory;

    private AppRoleProvidedBasedField(String fieldName, String fieldKey) {
        this.fieldKey = fieldKey;
        this.fieldName = fieldName;
    }

    private AppRoleProvidedBasedField(String fieldName, String fieldKey,
                                      boolean isMandatory) {
        this.fieldKey = fieldKey;
        this.fieldName = fieldName;
        this.isMandatory = isMandatory;
    }


    @Override
    public Boolean isMandatory() {
        return isMandatory;
    }

    @Override
    public VaultConfigurationType getVaultConfigurationType() {
        return VaultConfigurationType.APP_ROLE_PROVIDED;
    }

    @Override
    public String getFieldKey() {
        return fieldKey;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }


}
