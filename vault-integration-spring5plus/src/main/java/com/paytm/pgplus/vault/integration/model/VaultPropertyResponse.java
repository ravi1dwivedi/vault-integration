package com.paytm.pgplus.vault.integration.model;

import java.util.Properties;

public class VaultPropertyResponse {
    private boolean succes;

    private Properties properties;

    public VaultPropertyResponse(boolean succes, Properties properties) {
        this.succes = succes;
        this.properties = properties;
    }

    public boolean isSucces() {
        return succes;
    }

    public Properties getProperties() {
        return properties;
    }
}