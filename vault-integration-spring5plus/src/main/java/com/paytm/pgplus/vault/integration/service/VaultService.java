package com.paytm.pgplus.vault.integration.service;

import com.paytm.pgplus.vault.integration.condition.AutoPutPropertyInEnvCondition;
import com.paytm.pgplus.vault.integration.condition.VaultEnabledCondition;
import com.paytm.pgplus.vault.integration.config.VaultConfiguration;
import com.paytm.pgplus.vault.integration.config.builder.VaultConfigurationBuilder;
import com.paytm.pgplus.vault.integration.constant.Constants;
import com.paytm.pgplus.vault.integration.exception.VaultConfigurationException;
import com.paytm.pgplus.vault.integration.property.configurer.CustomPropertySourcesPlaceholderConfigurer;
import com.paytm.pgplus.vault.integration.util.PropertyUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.FileUrlResource;
import org.springframework.core.io.Resource;

import java.net.MalformedURLException;
import java.util.Optional;

public class VaultService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VaultService.class);
    private Resource vaultConfigurationLocation;

    private Optional<Resource> vaultPropertiesSecondaryLocation;

    private boolean isVaultEnabled = false;

    private boolean isAutoPutPropertyInEnvEnabled = true;

    public VaultService() throws MalformedURLException {
        vaultConfigurationLocation = new FileUrlResource("/etc/appconf/vault-config.properties");
        vaultPropertiesSecondaryLocation = Optional.of(new FileUrlResource("/etc/appconf/vault-read-failed.properties"));
        if (vaultConfigurationLocation.exists() && vaultConfigurationLocation.isReadable())
            this.isVaultEnabled = getVaultEnabledPropertyValue();
        else
            LOGGER.info("[VaultService] vaultConfigurationFile is not valid");
    }

    public VaultService(boolean isAutoPutPropertyInEnvEnabled) throws MalformedURLException {
        this();
        this.isAutoPutPropertyInEnvEnabled = isAutoPutPropertyInEnvEnabled;
    }

    public VaultService(Resource vaultConfigurationFile,
                        Resource vaultPropertiesSecondaryLocation) {
        if (vaultConfigurationFile != null
                && vaultConfigurationFile.exists()
                && vaultConfigurationFile.isReadable()) {
            this.vaultConfigurationLocation = vaultConfigurationFile;
            this.vaultPropertiesSecondaryLocation = vaultPropertiesSecondaryLocation == null
                    ? Optional.empty() : Optional.of(vaultPropertiesSecondaryLocation);
            this.isVaultEnabled = getVaultEnabledPropertyValue();
        } else
            LOGGER.info("[VaultService] vaultConfigurationFile is not valid");
    }

    public VaultService(Resource vaultConfigurationFile,
                        Resource vaultPropertiesSecondaryLocation,
                        boolean isAutoPutPropertyInEnvEnabled) {
        this(vaultConfigurationFile, vaultPropertiesSecondaryLocation);
        this.isAutoPutPropertyInEnvEnabled = isAutoPutPropertyInEnvEnabled;
    }

    @Bean(name = "vaultConfig", autowire = Autowire.BY_TYPE)
    @Conditional(VaultEnabledCondition.class)
    public VaultConfiguration vaultConfiguration() throws VaultConfigurationException {
        return VaultConfigurationBuilder.build(vaultConfigurationLocation);
    }

    @Bean(autowire = Autowire.BY_TYPE)
    @DependsOn("vaultConfig")
    @Conditional(AutoPutPropertyInEnvCondition.class)
    public CustomPropertySourcesPlaceholderConfigurer customPropertySourcesPlaceholderConfigurer() {
        return new CustomPropertySourcesPlaceholderConfigurer(vaultPropertiesSecondaryLocation);
    }

    public boolean isVaultEnabled() {
        return isVaultEnabled;
    }

    private boolean getVaultEnabledPropertyValue() {
        String propertyValue = PropertyUtil.getPropertyValue(vaultConfigurationLocation, Constants.VAULT_ENABLED);
        if (StringUtils.isNotEmpty(propertyValue))
            return Boolean.valueOf(propertyValue);
        return Boolean.FALSE;
    }

    public boolean isAutoPutPropertyInEnvEnabled() {
        return isAutoPutPropertyInEnvEnabled;
    }

}
