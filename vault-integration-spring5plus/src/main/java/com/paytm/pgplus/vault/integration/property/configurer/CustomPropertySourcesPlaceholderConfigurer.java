package com.paytm.pgplus.vault.integration.property.configurer;

import com.paytm.pgplus.vault.integration.config.VaultConfiguration;
import com.paytm.pgplus.vault.integration.model.VaultPropertyResponse;
import com.paytm.pgplus.vault.integration.util.PropertyUtil;
import com.paytm.pgplus.vault.integration.util.VaultPropertyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;

import java.util.Optional;
import java.util.Properties;

public class CustomPropertySourcesPlaceholderConfigurer extends PropertySourcesPlaceholderConfigurer {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomPropertySourcesPlaceholderConfigurer.class);

    private static final String vaultPropertySourceName = "vaultPropertySource";

    private static final String vaultPropertiesSecondarySource = "vaultPropertiesSecondarySource";

    private Optional<Resource> vaultPropertiesSecondaryLocation;

    public CustomPropertySourcesPlaceholderConfigurer(Optional<Resource> vaultPropertiesSecondaryLocation) {
        super();
        setIgnoreUnresolvablePlaceholders(true);
        this.vaultPropertiesSecondaryLocation = vaultPropertiesSecondaryLocation;
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) {
        VaultPropertyResponse vaultPropertyResponse = VaultPropertyUtil
                .fetchPropertiesFromVault(beanFactory.getBean(VaultConfiguration.class));
        Environment environment = beanFactory.getBean(Environment.class);
        if (vaultPropertyResponse != null && vaultPropertyResponse.isSucces()) {
            LOGGER.info("[postProcessBeanFactory] Vault response is successfull");
            PropertyUtil.addIntoEnvironmentProperties(environment, vaultPropertyResponse.getProperties(), vaultPropertySourceName);
        } else
            loadPropertiesFromSecondaryLocation(environment);
        super.postProcessBeanFactory(beanFactory);
    }

    private void loadPropertiesFromSecondaryLocation(Environment environment) {
        LOGGER.info("[loadPropertiesFromSecondaryLocation] Loading Property from secondary location");
        if (vaultPropertiesSecondaryLocation != null
                && vaultPropertiesSecondaryLocation.isPresent()
                && vaultPropertiesSecondaryLocation.get().isReadable()) {
            Properties properties = PropertyUtil.readProperties(vaultPropertiesSecondaryLocation.get());
            PropertyUtil.addIntoEnvironmentProperties(environment, properties,
                    vaultPropertiesSecondarySource);
        } else
            LOGGER.info("[loadPropertiesFromSecondaryLocation] Loading skipped as input file provided - {} is not valid"
                    , vaultPropertiesSecondarySource);
    }
}
