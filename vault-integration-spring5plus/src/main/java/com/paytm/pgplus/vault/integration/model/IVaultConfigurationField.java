package com.paytm.pgplus.vault.integration.model;

import com.paytm.pgplus.vault.integration.constant.VaultConfigurationType;

public interface IVaultConfigurationField {

    default Boolean isMultivalued() {
        return false;
    }

    default Boolean isMandatory() {
        return true;
    }

    VaultConfigurationType getVaultConfigurationType();

    String getFieldKey();

    String getFieldName();
}
