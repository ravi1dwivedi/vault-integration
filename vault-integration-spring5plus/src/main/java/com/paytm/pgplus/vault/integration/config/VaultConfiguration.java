package com.paytm.pgplus.vault.integration.config;

import com.paytm.pgplus.vault.integration.exception.VaultConfigurationException;
import com.paytm.pgplus.vault.integration.util.VaultConfigurationValidationUtil;
import org.springframework.vault.authentication.ClientAuthentication;
import org.springframework.vault.client.VaultEndpoint;
import org.springframework.vault.config.AbstractVaultConfiguration;
import org.springframework.vault.core.VaultTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

public abstract class VaultConfiguration extends AbstractVaultConfiguration {

    protected ClientAuthentication clientAuthentication = null;

    private VaultEndpoint vaultEndpoint = null;

    private URI endPoint;

    private Optional<String> dbPropertiesPath;

    private Optional<String> staticPropertiesPath;

    public VaultConfiguration(String endPoint,
                              Optional<String> dbPropertiesPath,
                              Optional<String> staticPropertiesPath) throws VaultConfigurationException {
        if (VaultConfigurationValidationUtil.validate(endPoint, dbPropertiesPath, staticPropertiesPath)) {
            try {
                this.endPoint = new URI(endPoint);
            } catch (URISyntaxException e) {
            }
            this.dbPropertiesPath = dbPropertiesPath;
            this.staticPropertiesPath = staticPropertiesPath;
        }
    }

    public final VaultEndpoint vaultEndpoint() {
        if (vaultEndpoint == null) {
            synchronized (this) {
                if (vaultEndpoint == null)
                    vaultEndpoint = VaultEndpoint.from(this.endPoint);
            }
        }
        return vaultEndpoint;
    }

    public final VaultTemplate getVaultTemplate() {
        return new VaultTemplate(vaultEndpoint(), clientAuthentication());
    }

    public Optional<String> getDbPropertiesPath() {
        return dbPropertiesPath;
    }

    public Optional<String> getStaticPropertiesPath() {
        return staticPropertiesPath;
    }
}
