package com.paytm.pgplus.vault.integration.util;

import com.paytm.pgplus.vault.integration.model.IVaultConfigurationField;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.*;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

public class PropertyUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyUtil.class);

    public static void addIntoEnvironmentProperties(Environment environment,
                                                    Properties properties,
                                                    final String propertySourceName) {
        if (environment != null
                && ((ConfigurableEnvironment) environment).getPropertySources() != null
                && properties != null && properties.size() > 0
                && StringUtils.isNotEmpty(propertySourceName)) {
            MutablePropertySources propertySources = ((ConfigurableEnvironment) environment).getPropertySources();
            PropertySource<?> dbPropertySource = propertySources.get(propertySourceName);
            if (dbPropertySource == null)
                dbPropertySource = new PropertiesPropertySource(propertySourceName, properties);
            propertySources.addFirst(dbPropertySource);
        }
    }

    public static Properties readProperties(Resource resource) {
        Properties properties = null;
        if (resource != null && resource.exists() && resource.isReadable()) {
            try {
                properties = new Properties();
                properties.load(resource.getInputStream());
            } catch (IOException e) {
                LOGGER.error("[loadPropertiesFromSecondaryLocation] Exception - ", e);
            }
        }
        return properties;
    }

    public static Properties getProperties(Map<String, Object> vaultPropertyMap) {
        Properties properties = new Properties();
        if (vaultPropertyMap != null)
            vaultPropertyMap.entrySet().forEach(
                    entry -> properties.setProperty(entry.getKey(), entry.getValue().toString()));
        return properties;
    }

    public static String getPropertyValue(Resource vaultConfigurationLocation, String propertyKey) {
        Properties properties = readProperties(vaultConfigurationLocation);
        if (properties != null)
            return properties.getProperty(propertyKey);
        return null;
    }

    public static String getProperty(Properties properties, IVaultConfigurationField field) {
        if (properties != null && field != null)
            return properties.getProperty(field.getFieldKey());
        return null;
    }
}
