package com.paytm.pgplus.vault.integration.util;

import com.paytm.pgplus.vault.integration.config.VaultConfiguration;
import com.paytm.pgplus.vault.integration.model.VaultPropertyResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.vault.VaultException;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.support.VaultResponse;
import org.springframework.web.client.ResourceAccessException;

import java.util.HashMap;
import java.util.Map;

public class VaultPropertyUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(VaultPropertyUtil.class);

    public static VaultPropertyResponse fetchPropertiesFromVault(VaultConfiguration vaultConfiguration) {
        VaultPropertyResponse vaultPropertyResponse = null;
        if (vaultConfiguration != null) {
            Map<String, Object> vaultPropertyMap = new HashMap<>();
            if ((vaultConfiguration.getDbPropertiesPath().isPresent()
                    && !fillDataFromVault(vaultPropertyMap, vaultConfiguration,
                    vaultConfiguration.getDbPropertiesPath().get()))
                    || (vaultConfiguration.getStaticPropertiesPath().isPresent()
                    && !fillDataFromVault(vaultPropertyMap, vaultConfiguration,
                    vaultConfiguration.getStaticPropertiesPath().get())))
                vaultPropertyResponse = new VaultPropertyResponse(Boolean.FALSE, null);
            else if (!vaultConfiguration.getDbPropertiesPath().isPresent()
                    && !vaultConfiguration.getStaticPropertiesPath().isPresent())
                vaultPropertyResponse = new VaultPropertyResponse(Boolean.FALSE, null);
            else
                vaultPropertyResponse = new VaultPropertyResponse(Boolean.TRUE,
                        PropertyUtil.getProperties(vaultPropertyMap));
        } else {
            LOGGER.info("[fetchPropertyFromVault] vaultConfiguration is null");
            throw new RuntimeException("[fetchPropertyFromVault] vaultConfiguration cannot be null");
        }
        return vaultPropertyResponse;
    }

    private static boolean fillDataFromVault(Map<String, Object> vaultPropertyMap,
                                             VaultConfiguration vaultConfiguration,
                                             String vaultPropertiesPath) {
        long requestTime = System.currentTimeMillis();
        try {
            VaultTemplate vaultTemplate = vaultConfiguration.getVaultTemplate();
            LOGGER.info("[getDataFromVault] Connecting to Path: " + vaultPropertiesPath);
            VaultResponse vr = vaultTemplate.read(vaultPropertiesPath);
            LOGGER.info("[getDataFromVault] successful with requestId: - {}",
                    vr != null ? vr.getRequestId() : null);
            if (vr != null && vr.getData() != null && vr.getData().size() > 0)
                vaultPropertyMap.putAll(vr.getData());
            vaultTemplate.destroy();
        } catch (ResourceAccessException rae) {
            LOGGER.error("[getDataFromVault] Exception - ", rae);
            return Boolean.FALSE;
        } catch (VaultException ve) {
            LOGGER.error("[getDataFromVault] Exception - ", ve);
            return Boolean.FALSE;
        } catch (Exception e) {
            LOGGER.error("[getDataFromVault] Exception - ", e);
            return Boolean.FALSE;
        } finally {
            LOGGER.info("[getDataFromVault] Response Time : {}.ms", System.currentTimeMillis() - requestTime);
        }
        return Boolean.TRUE;
    }
}
