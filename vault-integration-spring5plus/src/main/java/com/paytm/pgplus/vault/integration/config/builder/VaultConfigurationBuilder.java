package com.paytm.pgplus.vault.integration.config.builder;

import com.paytm.pgplus.vault.integration.config.AppRoleProvidedBasedVaultConfiguration;
import com.paytm.pgplus.vault.integration.config.VaultConfiguration;
import com.paytm.pgplus.vault.integration.constant.Constants;
import com.paytm.pgplus.vault.integration.constant.VaultConfigurationType;
import com.paytm.pgplus.vault.integration.exception.VaultConfigurationException;
import com.paytm.pgplus.vault.integration.model.AppRoleProvidedBasedField;
import com.paytm.pgplus.vault.integration.util.PropertyUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.Resource;

import java.util.Optional;
import java.util.Properties;

public class VaultConfigurationBuilder {

    public static VaultConfiguration build(Resource vaultConfigurationLocation) throws VaultConfigurationException {
        Properties vaultProperties = PropertyUtil.readProperties(vaultConfigurationLocation);
        if (vaultProperties != null) {
            String vaultConfigurationTypePropertyValue = vaultProperties
                    .getProperty(Constants.VAULT_CONFIGURATION_TYPE);
            VaultConfigurationType vaultConfigurationType = VaultConfigurationType.APP_ROLE_PROVIDED;
            if (StringUtils.isNotEmpty(vaultConfigurationTypePropertyValue))
                vaultConfigurationType = VaultConfigurationType.valueOf(vaultConfigurationTypePropertyValue);
            return build(vaultProperties, vaultConfigurationType);
        }
        return null;
    }

    private static VaultConfiguration build(Properties vaultProperties,
                                            VaultConfigurationType vaultConfigurationType) throws VaultConfigurationException {
        switch (vaultConfigurationType) {
            case APP_ROLE_PROVIDED:
                return getAppRoleProvidedConfig(vaultProperties);
            default:
                throw new VaultConfigurationException("vaultConfigurationType", "not handled");
        }
    }

    private static VaultConfiguration getAppRoleProvidedConfig(Properties vaultProperties) throws VaultConfigurationException {
        return new AppRoleProvidedBasedVaultConfiguration(
                PropertyUtil.getProperty(vaultProperties, AppRoleProvidedBasedField.VAULT_ENDPOINT),
                getOptional(PropertyUtil.getProperty(vaultProperties, AppRoleProvidedBasedField.DB_PROPERTIES_PATH)),
                getOptional(PropertyUtil.getProperty(vaultProperties, AppRoleProvidedBasedField.STATIC_PROPERTIES_PATH)),
                PropertyUtil.getProperty(vaultProperties, AppRoleProvidedBasedField.ROLE_ID),
                PropertyUtil.getProperty(vaultProperties, AppRoleProvidedBasedField.SECRET_ID));
    }

    private static Optional<String> getOptional(String value) {
        if (StringUtils.isNotEmpty(value))
            return Optional.of(value);
        return Optional.empty();
    }
}
