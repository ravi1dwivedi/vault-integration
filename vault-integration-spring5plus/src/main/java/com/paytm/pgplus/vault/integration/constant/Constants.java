package com.paytm.pgplus.vault.integration.constant;

public class Constants {

    public static final String VAULT_URI = "vault.uri";
    public static final String VAULT_ROLE_ID = "vault.roleId";
    public static final String VAULT_SECRET_ID = "vault.secretId";
    public static final String VAULT_DATABASE_PROPERTIES_PATH = "vault.database.properties.path";
    public static final String VAULT_STATIC_PROPERTIES_PATH = "vault.constant.properties.path";
    public static final String VAULT_ENABLED = "vault.enabled";
    public static final String VAULT_CONFIGURATION_TYPE = "vault.configuration.type";

}
