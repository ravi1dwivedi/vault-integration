package com.paytm.pgplus.vault.integration.config;

import com.paytm.pgplus.vault.integration.exception.VaultConfigurationException;
import com.paytm.pgplus.vault.integration.util.VaultConfigurationValidationUtil;
import org.springframework.context.annotation.Configuration;
import org.springframework.vault.authentication.AppRoleAuthentication;
import org.springframework.vault.authentication.AppRoleAuthenticationOptions;
import org.springframework.vault.authentication.ClientAuthentication;

import java.util.Optional;

public class AppRoleProvidedBasedVaultConfiguration extends VaultConfiguration {

    private String roleId;

    private String secretId;

    public AppRoleProvidedBasedVaultConfiguration(String endPoint,
                                                  Optional<String> dbPropertiesPath,
                                                  Optional<String> staticPropertiesPath,
                                                  String roleId,
                                                  String secretId) throws VaultConfigurationException {
        super(endPoint, dbPropertiesPath, staticPropertiesPath);
        if (VaultConfigurationValidationUtil.validateRoleAndSecretId(roleId, secretId)) {
            this.roleId = roleId;
            this.secretId = secretId;
        }
    }

    public final ClientAuthentication clientAuthentication() {
        if (clientAuthentication == null) {
            synchronized (this) {
                if (clientAuthentication == null) {
                    AppRoleAuthenticationOptions options = AppRoleAuthenticationOptions
                            .builder()
                            .roleId(AppRoleAuthenticationOptions.RoleId.provided(roleId))
                            .secretId(
                                    AppRoleAuthenticationOptions.SecretId.provided(secretId)).build();
                    clientAuthentication = new AppRoleAuthentication(options, this.restOperations());
                }
            }
        }
        return clientAuthentication;
    }

}
